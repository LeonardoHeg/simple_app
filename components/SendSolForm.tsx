import { useConnection, useWallet } from '@solana/wallet-adapter-react';
import * as web3 from '@solana/web3.js'
import { LAMPORTS_PER_SOL } from '@solana/web3.js';
import { FC, useState } from 'react'
import styles from '../styles/Home.module.css'
import data from './data/data.json'
import { Configuration, OpenAIApi } from 'openai';


export const SendSolForm: FC = () => {
    const [txSig, setTxSig] = useState('');
    const [res, setRes] = useState('');
    const { connection } = useConnection();
    const { publicKey, sendTransaction } = useWallet();
    const link = () => {
        return txSig ? `https://explorer.solana.com/tx/${txSig}?cluster=devnet` : ''
    }

    //const styles = {
    //    border: '1px solid rgba(0, 0, 0, 0.05)', 
    //};

    // const backend = async () => {
    //     await fetch('http://127.0.0.1:5000/api/endpoint',{
    //         'method':'POST',
    //         headers : {
    //             'Content-Type':'application/json'
    //         },
    //         body : JSON.stringify({
    //             "input" : data.prompt,
    //         })
    //     })
    //     .then(response => response.json())
    //     .then(data => setMessage(data.message))
    //     .catch(error => console.log(error))
    // }

    async function send() {
        const openAI = new OpenAIApi(new Configuration({
            apiKey : data.openai_key
        }));
        
        openAI.createChatCompletion({
            model : "gpt-3.5-turbo",
            messages : [{ role : "user", content: data.prompt + JSON.stringify(data.parameters)}]
        }).then(res => setRes(res.data.choices[0].message.content))
    }

    const sendSol = event => {
        event.preventDefault()
        if (!connection || !publicKey) { return }
        const transaction = new web3.Transaction()
        const recipientPubKey = new web3.PublicKey(data.public_key)

        const sendSolInstruction = web3.SystemProgram.transfer({
            fromPubkey: publicKey,
            toPubkey: recipientPubKey,
            lamports: LAMPORTS_PER_SOL * data.amount
        })

        transaction.add(sendSolInstruction)
        sendTransaction(transaction, connection).then(sig => {
            setTxSig(sig)
            var info = ""
            if(sig !== ""){
                send()
            }
        })
    }

    return (
        <div>
            {
                publicKey ?
                    <form onSubmit={sendSol} className={styles.form}>
                        <button type="submit" className={styles.formButton}>Send</button>
                    </form> :
                    <span>Connect Your Wallet</span>
            }
            {
                txSig ?
                    <div>
                        <p>View your transaction on: <a href={link()}>Solana Explorer</a></p>
                        <div>
                            <p>Sending prompt: </p>
                            <p>{data.prompt}</p>
                            <p>{JSON.stringify(data.parameters)}</p>
                        </div>
                        <div>
                            <p>Response:</p>
                            <p>{res}</p>
                        </div>
                        </div> :
                    null
            }
        </div>
    )
}